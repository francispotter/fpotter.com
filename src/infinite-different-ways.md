# Infinite different ways

(A concise guideline to success with Jira)

_Originally published on Medium in Oct 2018_

_For a more streamlined agile project management toolset, consider [GitLab Plan](https://about.gitlab.com/stages-devops-lifecycle/plan/)_

---

Jira is a powerful tool that can be configured in infinite different ways. It is tempting, and easy, to end up with an overwhelmingly complex configuration. For some companies and teams, the complexity is required. But it is expensive. In general, for a small team just starting out, a simpler configuration is better.

And consistency is key. If some work is managed as Subtasks while other work is managed at the Issue level, confusion will reign and progress will slow. It’s much better to have a simple set of policies, work to ensure everyone understands them, and encourage everyone to follow them.

## Why Jira?

Like other agile project tracking tools, Jira helps an organization answer certain questions:

- “What should I work on now?”
- “What is everyone else working on?”
- “What are the requirements for this work?”
- “How long will it take for some work to be completed?”
- “What changes are in an upcoming or past release?”

Effective and consistent use of Jira can drive rapid, highly disciplined development, but less effective use can result in tedious administration, poor prioritization, and ineffective teams.

Jira is not the only agile project tracking tool, but it is by far the most widely used. Atlassian (the company that makes Jira and Confluence) has a market capitalization of US $12 billion, and it’s estimated that their tools are used by 43,000 organizations (including 80 of the Fortune 100).

## Agile Process

Improving the use of Jira is most helpful in the context of adopting a disciplined agile process, and it would be irresponsible to dive into Jira recommendations without first addressing the need for overall process change.

One of the most important aspects of running a cost-effective development process is managing meetings. Meetings are fundamentally expensive, and when time in meetings is spent in confusion, or in discussion of topics that are only of interest to one or two people, it is a drain on the overall effectiveness of the team.

If your team has a habit of holding daily calls and going through lists of tickets during the calls with everyone on the line — or similarly time-wasting practices — it’s time to break the habit.

We recommend adopting a tight agenda for daily meetings that does not include ticket review.

Typical agile teams organize their work into iterations (sometimes called “sprints”) and hold an iteration planning meeting (sometimes called “sprint planning meeting”) at the beginning of each cycle. That’s the best time to talk about requirements in detail. It should happen once per iteration (i.e. every week or every other week, depending on the release plan), not every day.

We recommend holding regular, planned, disciplined iteration planning meetings.

Daily calls are important, but are best when they follow a “standup” agenda, in which a facilitator (typically but not necessarily the project manager) asks each person in turn to quickly answer 3 questions:

- What did you work on yesterday?
- What are you working on today?
- Do you have any blockers or questions for others in the meeting?

If someone does need to ask about a specific ticket in a daily meeting, it should be a quick question about status (i.e “John, did you finish BPH-61922?”) followed by a quick answer (“yes!”). The daily meeting is NOT the time to talk about the requirements or technical details of individual tickets. If a developer needs more insight than the ticket description provides, they can take that conversation offline with whichever product specialist is involved in that particular ticket.

To keep the team organized and the daily calls short, the project manager should be continuously going through the tickets outside the meetings, gathering information from involved individuals about their status, and updating the tickets as necessary. The project manager should know the status of everything and should be able to quickly address questions about ticket status.

Iteration planning meetings are also structured, but take longer. Some organizations allocate a whole day for iteration planning; others try to keep it to an hour. At the iteration planning meeting, the “product owner” (typically a product manager, or even the CEO, but preferably not one of the developers) present new Features that they would like to have addressed in the upcoming iteration — in priority order. The developers can ask questions about each Feature. In addition, open Defects, Tasks, and Questions are reviewed for inclusion in the iteration. The goal is to have a fairly cohesive plan for the iteration by the end of the meeting.

The project manager should prepare for the iteration planning meeting, by consulting with the product owner to make sure ticket priorities and descriptions are ready ahead of time. In addition, the project manager performs a series of followup tasks after the meeting, including labeling the Issues to be included in the iteration with the correct Fix Version (see below).

## Jira Projects

The largest unit for organizing work in Jira is a Project. Separate Jira Projects are useful if there are different Issue Types, different custom fields, or different workflows. But otherwise, it’s easier to keep all the work for one code base and one team in a single Project. That way, it’s easy to manage Boards, Versions, and Filters within that Project.

Most importantly, if a single code base is managed in multiple Projects, it’s hard to keep track of what changes went into which releases. That’s because the Version field has to be exactly identical between the different Projects — something that’s difficult to keep in sync.

Note that moving tickets between Projects in Jira can be tricky and requires mapping any fields or statuses that are different. If the same Component or Version exists in both Projects, then that information will be copied, but if they are different, they have to be mapped too.

If you have too many projects and want to consolidate (a typical scenario), then it is tempting to “start fresh” with a new Project. But doing so would require copying all the tickets, which should be avoided. It’s probably better to adopt one of the existing Projects and archive the others. Just pick the one with the most tickets in an active status, then migrate active tickets from the other Project(s) in question.

Jira Project are rarely deleted; the appropriate action is to deactivate them, which can be done in a way that leaves it viewable but neither editable nor able to accept new tickets.

Many organizations use separate Projects in Jira for completely different activities. For example, one company we spoke with has a Jira Project for tracking candidates for hiring, and one entirely devoted to addressing runtime “Incidents” such as downtime, complicated customer support events, or anomalies in logs and monitoring systems. Your main product development Project should be for development work, not for tracking the troubleshooting of runtime incidents.

## Jira Boards

Jira Boards are a useful tool for visualizing and organizing work. A Board is more than just a Filter showing a list of tickets, in that it allows drag-and-drop serial ranking of tickets to designate the order in which developers and testers should work on them.

Some people think that a Board is the same as a Project, but that’s not the case. It’s possible to have more than one Board per Project, which might be useful for different subteams or initiatives. So it is not necessary to create a new Project just to have a different Board.

(It’s also possible to have a Board across more than one Project. But that’s complicated. The left navigation is specific to a single project, so can be lost — or confusing — if the Board covers multiple Projects.)

Boards can be configured with a Filter (which Issues to include), columns (usually statuses from the Workflow) and swimlanes (usually Components or some other Issue organization). Personally, I’ve found the swimlanes feature to be visually confusing, because it forces important work “below the fold” and requires a lot of scrolling.

Project managers should decide when and if to create new Boards. Just keep it simple and don’t overdo it ;-)

## Jira Issues and Issue Types

The main unit of work in Jira is the Issue.

Jira supports different Issue Types, and thoughtful configuration of the types is critical to smooth team execution and minimal confusion. Besides a name and a handy icon, each Issue Type has its own Workflow, so it is useful to define them clearly and understand their behavior. And it’s important to keep them simple.

We recommend Configuring your dev Projects with 4 simple Issue Types:

- **Feature**: New functionality (sometimes called “story” or “improvement”) — the description should describe the requirements for the feature in precise terms.
- **Defect**: Something that is not working as specified or as a typical user would expect it to work. Could be a defect in production or in an upcoming release (sometimes called “bug”) — the description should include numbered steps to reproduce the bug.
- **Task**: Something that requires the development team to do some work that does not result in a user-facing change. Note this is different from Subtask (which is a part of an issue).
- **Question**: Requires discussion but no actual coding. Does not relate to a release (i.e. no FixVersion).
Ideally, the iteration planning meeting involves reviewing well-written tickets at the Issue level.

Use Issues wisely. A piece of work is an Issue if it…

- Can be defined, described, and understood by the product owner, or someone outside the development team
- Is work for the development team
- Can be completed for a specific release
- Has a clear written definition of what it means to be finished

In a product development Project, avoid adding Issues for…

- An ongoing concern that’s investigated over a long time (might be an Epic)
- An incident that happened once and needs troubleshooting (might go into a separate Incidents project)
- A test or other task that’s only for the QA team (might be a Subtask)

It’s the project manager’s job to help everyone (most importantly, the product owner) break their needs down to the Issue level and complete the types and descriptions prior to the iteration planning meeting in a way that is consistent with the norms of the project.

## Jira Epics and Subtasks

An Epic is a group of Issues that go together into a major effort and may spread across several releases. Subtasks provide a way to break apart the work within an Issue to assign to different developers, track progress, and aid in estimating. Both are entirely optional.

While it’s tempting to make broad use of Epics and Subtasks, it can also be confusing. The tickets have distinct workflows; Jira does not automatically update a “parent” when the “child” updates. So Boards can easily get cluttered with, for example, Issues whose Subtasks are also present on the Board; or worse, Issues whose Subtasks are all complete and the Issue itself is a zombie. But our goal is to keep things simple, remember?

We recommend using Issues for all code changes, using Subtasks only within the dev team, and using Epics sparingly if at all.

If a project manager wants to create an Epic to contain a set of Issues across a long time period, they can; but keep the Epic itself off the Boards that the team are using every day (or make a Board only for that Epic).

Likewise, if developers want to break an Issue down into Subtasks to aid their day-to-day work, great! But the Subtasks are for developer use only and not to be shared with the product owners. A Subtask should not go through a testing process of its own; it should be a simple task that is just open or complete. If an Issue has Subtasks, then when all the subtasks are complete, the developer can mark the Issue as complete and ready for testing.

There is another Jira feature, widely used in some organizations but not in others, which is Story Points — a way to estimate work numerically. If you need to provide accurate and precise timeline estimates, Story Points can be a useful tool, but using them requires significantly more work on everyone’s part, and is only recommended for teams that have reached a certain level of sophistication.

## Jira Priority

Priority is a required field in Jira and is widely used to indicate which items need the most urgent attention.

It is tempting, and typical, for the person who creates a ticket to give it a high priority, since that request seems urgent at the time it is created. But very few things are truly urgent. If the production site is suffering from a customer-facing bug, that’s urgent. Otherwise, urgency is relative.

We find it useful to reserve the very highest priority level (“P0” in our case) for Defects (not Features or Tasks) that are present in Production and must be hotfixed. The other Priorities can be applied to work in development or QA for the upcoming release.

We recommend reserving the highest Priority ONLY for customer-impacting live production bugs requiring a hotfix.

It’s important that each Priority level have a simple, objective, written definition, so everyone can easily discern which Priority level to apply to a ticket. Prioritization should naturally follow a reasonable curve; not everything is a P1; ideally there should be plenty of “medium” and “low” priority items. But it’s also important that prioritization reflect a reasonable workload; it should be possible to complete some of the “low” priority items sometimes, or nobody will ever assign that Priority.

It is possible and helpful to add a link in Jira, next to the Priority field, with definitions on Confluence. (It’s a hack of sorts; in the field configuration, it’s possible to put HTML in the description; just remember target=_blank.)

It’s also helpful to create custom icons for the priorities; the default icons are a little odd and hard to understand at a glance. That’s something to consider.

Here’s a proposed breakdown of Priorities that we recommend for many teams:

- **P0/Emergency**: Only for Production Defects that must be fixed immediately with a hotfix
- **P1/Critical**: Only for Defects, including in Production or Dev/QA, that absolutely must be completed for the next release or it will be broken
- **P2/High**: Typically only 1–2 per release, Features that are considered core to the requirements for the release.
- **P3/Medium**: Other Features that are important for the release, but might not get completed
- **P4/Low**: “Nice-to-haves” that should be addressed if they are low effort or the developers have time

## Jira Workflow, including Status and Assignee

The core of Jira’s capability — both its secret superpower and it’s capacity for evil — lies in its configurable Workflow.

Here, more than anywhere else, it’s critical to keep things as simple as possible. (We heard of one project with 60,000 tickets and only one Workflow for the entire project — now that’s simplicity!)

If your Workflows are out of hand, reconfigure them, but do it thoughtfully. The first step toward configuring a Workflow is to diagram the states and transitions, and ensure that critical team members have the opportunity to provide input to the diagram. It’s important that everyone understand the states and transitions well enough to use them consistently.

We recommend drawing and agreeing on state diagrams for all Workflows.

Workflow can (and should) be configured differently for each Issue Type; here are some thoughts about each Issue Type described above:

- **Feature**: A full workflow including distinct steps for QA and product owner verification (also called “UAT” or “acceptance testing”).
- **Defect**: A slightly shorter workflow which includes QA but NOT product owner verification; it is assumed that the defect is described in such a way that a QA engineer can confirm it is working correctly.
- **Task**: A very simple workflow that might only include “open”, “in progress”, “done”, and “closed” states, with the option for the developer to move it directly from “done” to “closed” if it doesn’t require verification.
- **Question**: Even simpler workflow, maybe just “asked”, “answered”, and “closed”. The answer to the question can go into the comments.

Draw workflow diagrams so that they flow from top-left to bottom-right, so the direction is instinctively clear to everyone looking at them.

It is possible to configure workflow transitions in a way that are restricted to certain people or groups. For example, you could configure a Project such that only a QA engineer can move a ticket from an “in QA” state to a “done” state. We generally recommend against doing so. It can be confusing when different people see different buttons, and annoying when someone knows they are doing the right thing but can’t do it. It’s better to have good training and a sense of trust than to enforce security policies within a team.

Once the Workflows have been configured in Jira, it’s possible to allow team members to view an automatically generated workflow diagram from the Issue pages. This feature is not enabled by default, but is easy to enable, and we don’t see why it’s a bad thing to do, ever. It’s a permission setting called “view read-only workflow” which causes a link to the diagram to appear next to the status name. Do that!

## Jira Version

Jira supports the idea of Version, but the actual implementation is intentionally vague. The options for Version are configured at the Project level. That’s why it makes sense to consolidate work that’s on the same release cycle into the same Project in Jira.

But Jira uses the Version field in two places — “Affects Version” and “Fix Version”. Atlassian defines them in their documentation ([link](https://confluence.atlassian.com/jira064/what-is-an-issue-720416138.html)):

- **Afffects Version**: Project version(s) for which the issue is (or was) manifesting
- **Fix Version**: Project version(s) in which the issue was (or will be) fixed

A widely linked (and slightly more helpful) online discussion ([link](https://community.atlassian.com/t5/Jira-questions/Fix-Version-vs-Affects-Version/qaq-p/245745)) defines them this way:

- **Affects Version**: “This is where we found this issue”
- **Fix Version**: Starts off as “version we plan to fix this issue in” then after getting closed it becomes “version we actually fixed this issue in”

Adaptavist ([link](http://www.adaptavist.com/w/best-practice-for-jira-fields/)) says simply:

> Fix Version field is used to indicate the future version in which the issue will be resolved.

These definitions are consistent with the way we’ve seen these fields used, although some teams are creative or lazy about their use. Most importantly, you need an easy way to search for Issues representing work that you expect to include in an upcoming release. The filters for that can become quite complex if you aren’t keeping your Versions up-to-date.

We recommend using the Fix Version field as designed, to easily list changes slated for an upcoming release.

This means applying the Fix Version field to tickets prior to their actual release. In fact, _this should happen right after the iteration planning meeting_. Then the Fix Version becomes the plan for the release. If, during development or QA, it becomes apparent that a ticket will not make it into the planned release, then it must be removed from the Fix Version (some teams refer to this as “punting”).

This way, it is _always_ possible to see what Issues are expected to be included in the upcoming release (or a future release) by filtering a search or creating a release-specific Board.

The Affects Version should really only be used for bugs. Here’s how it works. Imagine that I notice that the Sign In button is broken on the live site. I file a P0 Defect (because it’s impacting production) and I look up the release number of the live site, and put that release number as the Affects Version. From that, we know when that bug (presumably) first appeared in the site. When it’s fixed, and the Fix Version field shows which release the fix went live, then we can easily figure out how long that bug was in production (and, if necessary, correlate it to traffic patterns, sign-ins, etc).

So the Affects Version is optional, only really applies to Defects, and doesn’t change once it’s entered (unless you realize that a bug was actually introduced earlier than first thought, in which case it might be changed). It’s helpful to developers when tracking down an issue, and to the product owner when figuring out the impact of a problem, but it’s not a critical component of the workflow.

It’s _possible_ to configure the forms in Jira so that Affects Version only shows up for Defects, but configuring different forms is tedious and complex. What’s easier is to set up the field configuration at the Issue Type level to hide it for all the other issue types.

Finally, note that the Fix Version field is meant to be a permanent record of the Issues included in a past release. So it is bad practice (and ought to be prevented in the workflow) to reopen Features and Bugs after they have been released. If it’s discovered that a Feature or Bug really wasn’t addressed (and you though it was), then it can be reopened, but then the Fix Version field must be cleared or changed to a future release. More likely, a post-release problem is found in the new feature. In that case, it’s much better to open a new ticket (possibly a P0 Defect) rather than reopen a Feature that is mostly complete. This way, it’s always possible to search for the tickets included in a past release.

## Jira Resolution

Always present, and ever confusing, Resolution is more than a field — it is also a switch. If there is any value in the Resolution field, the ticket is assumed to be “resolved” in various reports (including, for example, the default “My Open Issues” filter).

Resolution is required when it is present, which is why it should only appear on the form for the final transition of a workflow. And, if a ticket is reopened after being finalized, it is critical that the resolution be cleared. This can be automated.

To add to the confusion, the Resolution field has nothing to do with a “Resolved” status or a “Resolve” transition, which might appear in a Workflow (and actually appear by default).

From Adaptavist ([link](http://www.adaptavist.com/w/best-practice-for-jira-fields/)):

> Whilst the Status field shows where the issue or task is in the current workflow, Resolution is the flag that tells JIRA whether or not it is considered resolved.

As long as the Resolution field is empty with no value selected, the issue is deemed open. However, if the field is set to anything else, JIRA considers the issue resolved, displays the issue ID with a strikethrough and removes it from the standard reports.

Such, then, is the nature of Resolution that due care and attention is required when designing forms and workflows using this field. Here are the key points to remember:

Never add the Resolution field to the Create or Edit screen, unless you genuinely require issues to be created already resolved.

In your Resolution field options, never include values such as “Unresolved”, “Open” or “Unfinished”, since JIRA will simply read any such value as indicating the issue has been resolved (regardless of what you name it).

If you do break the Resolution field as described above, you may well need to repair issues by clearing their resolutions. Whilst this can certainly be undertaken by administrators, the potentially tricky process for doing so — removing the erroneous value from the Resolution list, updating the issue, amending the workflow and editing the relevant table in SQL — means it is all the more vital that you avoid problems in the first place.

Furthermore, always make sure JIRA is offline and backed-up before undertaking any remedial work — and remember to re-index afterwards.

## Jira Component

The Component field exists by default in Jira, and is used differently by different teams.

From Adaptavist ([link](http://www.adaptavist.com/w/best-practice-for-jira-fields/)):

> The Component field enables the grouping together of issues into smaller parts. Be aware that components are not global fields and cannot be shared across multiple projects. Instead, if you need to share components more widely use a globally-defined custom Multi-Select List field.

Components provide extra flexibility and convenience in that project-level administrators can manage field lists themselves without needing to revert to their JIRA administrators.

One other key benefit is the ability to nominate “Component Leads” and have JIRA automatically assign the relevant issues to them when the component is selected.

This last part of Component functionality — the leads — is what makes the Component field different from a custom field. It’s possible to auto-assign tickets to the Component lead, on a component basis. So the lead for Component A can have their tickets assigned to them, but B can leave theirs unassigned. It’s also possible to auto-notify the component lead on any kind of event, but that configuration is on the project level. And of course it’s possible to have Components without Leads.

Component Leads might be a little too complex for small teams, but use Components to divide your system into broad areas (potentially aligning with folders/modules/etc within the code base). Keep the list short — no more than 6–8 Components — or it will become unweildy and useless.