# Tips for engineers entering sales

The "10 Rules of Selling" I wrote down after [working with a guru](engineers-become-salespeople.md):

1. Greet every day with love in your heart
1. Have an audavious purpose and force yourself to believe in it
1. Be interesting ~ never talk about what you're selling
1. Know more about your customers' business than they do
1. Always start with the CEO
1. In all cases, take the boldest path
1. Stay calm when others become uncomfortable
1. Find out where the line is by crossing it
1. Kiss ass
1. Sell high

My reading list for engineers (or really anyone) entering sales:

1. Power Questions (Sobel, Panas)
1. Getting to Yes (Fisher, Ury, Patton)
1. How I Raised Myself… (Bettger)
1. The Seven Habits… (Covey)
1. Mindset (Dweck)
1. Clients for Life (Sobel, Sheth)
1. Never Eat Alone (Ferrazzi, Raz)
1. How to Work a Room (RoAne)
1. Think and Grow Rich (Hill)
1. The Greatest Salesman in the World (Mandino)

_See ["Selling is so easy!"](engineers-become-salespeople.md)_
