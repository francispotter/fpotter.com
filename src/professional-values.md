# Professional Values

While attending Carnegie Mellon University in the late 1980s, I attended two on-campus presentations by author Kurt Vonnegut. At the time, the university received large research grants each year from the US military to conduct research in support of the Reagan-era defence buildup. Vonnegut pleaded with students to adopt an ethical standard in their careers, even going to far as to suggest creating the equivalent of a “hippocratic oath” for engineers, and specifically to refuse work in the military sector.

Inspired by this call to ethical practice, I started my career guided by strong values and social responsibility. Shortly after graduation, I worked for several years in the international social justice sector, including service to community organizations on the ground in Southern Africa. Since joining the tech sector, I've strived to cultivate relationships with organizations and clients whose missions align with my core principles. Specifically, I actively seek to work with others who:

- Commit to environmental stewardship and sustainable practices
- Champion diversity, equity, and inclusion, especially toward women, international populations, and LGBTQ+ individuals
- Work to build a more just and peaceful world through innovation, education, and social progress

My standards reflect years of thoughtful consideration about how my skills can best serve society. This approach has led to deeply meaningful collaborations that advance technical excellence, social good, and personal fulfillment.
