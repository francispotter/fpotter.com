# The importance of dark mode

_An appeal to professional associates, first posted to LinkedIn, March 2023_

## Survivor

In November 2022, doctors diagnosed me with meningioma - a "benign" form of brain tumour. Four days later, surgeons removed it. My functional and professional abilities returned after recovery, but the tumour and surgery left me with a persistent mild visual impairment.

I see the world 24/7 through what looks like a transparent layer of white static, like a mistuned analogue television. Neuro-ophthalmologists call the condition "visual snow". It's probably permanent.

The impairment has a minor impact on workplace interactions, and I appreciate accomodation by colleagues and associates.

## What is dark mode?

Most people spend most of their screen time working on a white background.

<img src="assets/light-mode.png" width="100%"/>

The view shown above blinds me, slows me down, and causes traumatic headaches. "Dark mode" works much better.

<img src="assets/dark-mode.png" width="100%"/>

I've found that many people prefer "dark mode" but for me, it's necessary for productivity and health.

## How to help

When sending a screenshot or sharing your screen on a Zoom call, please consider setting up dark mode ahead of time.

You might appreciate giving your eyes a break, even for everyday work, and it would certainly help when sharing with me.

### Chrome extension

The [Dark Reader](https://chromewebstore.google.com/detail/dark-reader/eimadpbcbfnmbkopoojfekhnkhdbieeh?hl=en) Chrome extension works well, and has plenty of settings for sites with different types of CSS stylesheets. If you install the extension, but prefer to use light mode for daily work, just turn it off until our next call.

Dark Reader automatically detects dark mode in site and apps that already support it (such as GitLab), so you can take advantage of app-specific dark mode design when available.

### GitLab

Turning dark mode on and off in GitLab is easy. Follow these steps:

1. Click on your profile photo near the top left of any page
1. Choose "Preferences" (or click [here](https://gitlab.com/-/profile/preferences) while signed into GitLab.com)
1. Under "Appearance", choose "Dark (experiment)"
1. Under "Navigation theme", choose "Neutral"
1. Under "Syntax highlighting theme", choose "Dark"

### Presentations and more

Also consider using a dark background for presentation slides and other applications that support it.  Other techniques include:

Some other possibilities:

- [Dark theme](https://support.google.com/chrome/answer/9275525) in Chrome itself
- [Dark appearance](https://support.apple.com/en-ca/guide/mac-help/mchl52e1c2d2/mac) setting in MacOS
- [iTerm](https://iterm2.com/documentation-preferences-appearance.html) offers a dark appearance option

Most of the settings described above work in similar ways on other operating systems, browsers, and terminal emulators.

I deeply appreciate all the support I've received from my professional community since the surgery. Thank you.

## Note to designers

Web and UI designers, take note! The extra effort put into "accessibility" makes a real difference to some users. For example, test your web app with the Dark Reader extension to make sure UI controls remain visible. If you want some free feedback on your site and application designs from someone who needs the accommodation, just let me know.

<br/><br/>

_See [Surviving a Brain Tumour](tumour-snow-email.md) for the whole story_