# Surviving a brain tumour

_The story of my brain tumour and recovery experience, adapted from an email sent to family and friends, January 2023_

In the early evening of November 10, 2022, in a small room in the emergency area of North Island Hospoital Comox Valley, a doctor entered and showed me a picture of the inside of my brain. My world changed in an instant.

A few months earlier, I’d been living in oppressively hot Santa Clarita, California with my wife and her teenagers. We were married in 2020 in a tiny backyard ceremony with only two guests, moved to Canada a year later, settled in a gorgeous small town on Vancouver Island called Courtenay, and bought a house.

During the same period, I made 5 visits to England to visit my dying aunt Jane. I retrieved her from the hospital in 2020 and brought her to our decades-old family home in Wolverhampton, near Birmingham. I then helped her take a place in a top-quality care home nearby, where she stayed until she died surrounded by lifelong friends in March 2022. I organized and spoke at Jane's funeral, and cleared out the house for sale.

Feelings of lethargic and lazyiness increasingly took hold, both in England and at home in California. I attributed my condition to the COVID environment, the weight of travel, the sadness of working with my dying aunt, getting older, bad habits, and life stresses. Still, I ended up having to leave my job,  and could barely function at home. 

Hoping that the new environment would provide a fresh start for me, I embraced Canadian life. But decline continued. I had memory lapses; the month of October has more or less disappeared from my memory. Headaches recurred every day. I had a dizzy spell in the public library and fell on the floor. One day I walked around in a daze for hours, unsure where I was, in my own home. But still we couldn’t identify the problem, and neither could doctors.

In November the headaches became more severe early in the mornings. I read Wikipedia.com, and learned a little about brain tumors. The descriptions of headaches and other symptoms uncannily seemed to match, and I knew the time had come to take the problem seriously, whatever it was.

My wife drove me to the hospital, where the doctors finally performed a CT scan, then diagnosed the tumour, immediately began treatment and scheduled emergency surgery.

The surgery succeeded. The tumour is gone.

I’m lucky. Meningioma is a slow-growing type of brain tumour, different from the glioblastoma that kills so many people. One surgeon said it had probably been growing in my brain for “years”, which explains why the symptoms seemed to accumulate so slowly. If the surgeons missed a crumb, it could grow back, probably just as slowly. So I will be receiving follow-up MRI scans, possibly for the rest of my life, to keep an eye out for recurrence.

In addition to the surgery, I received a 3-week steroid treatment (Dexamethasone), which prevented me from sleeping, giving plenty of time to think about the past and the people in my life. I also enjoyed long, rich conversations with my wife, sometimes through the night, about our life together and hopes for the future. A pleasant kind of emotional healing took place.
 
A physical therapist helped me re-learn to walk, climb stairs, and balance. Family and friends provided immense levels of support. It took faith, courage, and patience, but I finally felt ready to re-enter the social and professsional world about 6 months after the diagnosis and surgery.

_See [A Vision of Static](tumour-snow-vsi.md) about the permanent vision impairment that followed the brain tumour experience_