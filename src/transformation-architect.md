# The Transformation Architect

"Training Coordinator" sounded like a mundane job title for a university engineering graduate, but I embraced the role with curiosity and drive. Some of the "training" I coordinated covered technical topics (what substances corrode stainless steel?) while most touched on "soft skills" — professional writing, team building, effective meeting management, the "psychology of achievement", and so on. From sitting in the backs of classrooms for a year, I gleaned many tips and tricks that have helped me master a variety of situations throughout my professional life in engineering, management, and consulting.

But even better, I got to work with a man named Frank.

Together, Frank and I formed part of the "Total Quality Management" group at a large global corporation. TQM had taken the business world by a storm following the well-documented success of Toyota in the automobile market, and businesses worldwide tried to emulate their approach. An internal consultant nearing retirement, Frank took me under his wing and let me shadow him during my year on the team.

Frank advised executives throughout the company who struggled with business challenges, from simple questions ("too many people taking sick days") to large, demanding projects ("we missed our production targets last quarter"). 
Often, Frank's "customers" expected answers. They wanted a quick fix, a ready-made solution, or justification for a tough call. They hoped Frank would tell them who to fire, how to justify workload increases, new purchases to authorize, or what new procedure to dictate.

But Frank had different ideas. As a champion of organizational change, he believed that the best decisions and most effective changes come from inside. "Whoever shows up for a meeting," Frank would say "is the right group of people. Inspire them to bring their best thinking, and facilitate an effective decision."

That year, I learned that, to meet the challenges of changing markets, business conditions, and demographics, organizations must change constantly. And that change must happen across 5 dimensions, often in parallel. Conveniently, the titles all start with "S".

- **Strategy**: Clear goals and objectives combined with meaningful organizational values 
- **Skill**: Knowledge and abilities of individuals
- **Styles**: A generative culture that fosters both autonomy and collaboration
- **Structure**: Appropriate job titles, reporting relationships, and incentives
- **Systems**: Tools and technology that underlie successful processes

Need to set a new direction? That's a strategic change. The group might need new skills to implement it. Want to fire someone? That's structure, but watch out for communication style gotchas. All five dimensions matter.

With each internal "client", Frank started by scheduling a transformation workshop with a cross-section of the team. He showed up with a simple poster covering "ground rules" and a loosely defined process. Then he started facilitating -- neither instructing nor simply watching -- a profound process of brainstorming, refinement, prioritization, and, ultimately, design for new versions of each of the 5 S's.

Rooms buzzed with creative energy and enthusiasm as individuals broke from their daily grind to brainstorm and collaborate in a deep, powerful way. Frank unleashed the genius within every individual in the otherwise boring corporate environment, as ideas flowed. He gently facilited the process, nudging the group toward active listening, thoughtful prioritization, and ultimately the definition of a better organization for themselves. He believed that, with the right guidance, teams could overcome challenges most effectively through grassroots transformation.

The process worked. Entire corporate departments accomplished goals they had set out for themselves, executives kept Frank's number on "speed dial" (consider the era), and ultimately the corporation continued to dominate its industry over the successive decades.

Years later, "Six Sigma" replaced TQM. Then, as enterprise software transformed the world of business entirely, agile and lean methodologies became popular. I've read many of the books and seen the newer techniques in action. Despite changes in technology, culture, and language, the principles that I learned from Frank remain timeless.

1. Organizations can change to meet new challenges and expectations (or even exceed them)
1. The best people to drive change are the people doing the work - from all levels
1. Effective techniques of advice, facilitation, and coaching can have an impact on outcomes
1. Keep the 5 S's in balance throughout the process

Frank retired, and died shortly thereafter. I moved on to the next stage of my career. Years later, after moving through various roles as individual contributor, manager, and consultant, I decided to practice techniques and adopt values learned from Frank no matter what job title I hold. For myself, my colleagues, and my customers, I hope to facilitate lasting multidimentional change in the hearts and souls of organizations the way Frank demonstrated all those years ago.
