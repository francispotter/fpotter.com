# A vision of static

_The story of my vision impariment, adapted from an [article](https://www.visualsnowinitiative.org/warriors-of-the-week/francis-potter/) written for the Visual Snow Initiative_

I walked into the store, and everything looked greyish. Colours still existed, but seemed somehow muted or less vibrant. Perhaps the store had chosen cheap lighting, I thought. Or my eyes had to adjust to the oncoming winter. Or I needed new glasses. In fact, I had experienced the first hint of my journey with "visual snow".

Visual problems appeared on my list of complaints when I entered the emergency room about 3 months later, along with severe headaches, mental confusion, dizziness, and memory issues. “Meningioma” they called it: a type of brain tumor. Four days later, a surgeon removed the tumor, and the long recovery began. But things went sideways quickly. After I came home, my vision started to decline rapidly until, on Christmas eve, I returned to the hospital for a follow-up scan. Apparently, my brain had started a healthy healing process, and everything would work out fine.

But the problem persisted. Colours appeared less vibrant, as I had noticed in the store. But I also saw static — like an old-fashioned television with a broken antenna — across my entire visual field, distorting everything and bringing a kind of unwanted action to even the calmest views. Reading became nearly impossible, especially on paper or white backgrounds, and I struggled to recognize friends in crowded rooms or see pedestrians on the road.

My condition stumped all the doctors. The neurosurgeon said he had operated in the front of my brain, far from the visual cortex, so I must have an eye problem. The optometrist prescribed new eyewear and claimed he had returned my vision to 20/20. The ophthalmologist saw no evidence of acute eye disease and asked me to return in a month. The oncologist said things happen, and I ought to find a new career since I might have a hard time working with computers ever again. My family doctor shrugged.

Confused, lonely, and scared, I finally phoned a family friend who had retired from his optometry practice (and patent-earning optometric research) decades ago but still knew a thing or two. Within five minutes, he named my condition: “visual snow”.

The physical snow had begun to pile up outside in the Canadian winter when my wife drove me for over an hour to see the nearest neuro-ophthalmologist (yes, that is a real profession, as I learned). The doctor explained -- as I had already read -- that my condition had no cure. He prescribed a medication to try (useless, I learned online before declining to take it) and petitioned the province to revoke my driving license.

Determined to figure out a way forward, I dove into online research, my laptop set to light-on-dark text, the only way I could read. I learned that visual snow is widely misunderstood, often misdiagnosed, untreatable, and usually permanent. The biggest impact for many of us is depression because of the dissociation and isolation that comes from losing visual sense. I decided to keep my family and friends close, practice adaptive strategies, and maintain a sense of hope.

Then the Visual Snow Initiative appeared in my web browser. I cried, literally, as I read the web site and watched the founder's TED talk. Finally, it seemed that someone understood. Based on the work of the Initiative, I underwent a course of vision therapy, which did little to diminish the static in my visual field, but helped me develop confidence and function better in the new world I occupy.

Did the brain tumour cause my visual snow? Or the surgery? The powerful 3-week steroid treatment? Some combination of them? Or pure coincidence? I will always wonder. The day the doctors diagnosed my brain tumor seems like a rebirth to me, and I plan to celebrate and enjoy my second chance at life, even with all this static in my view.

_See [the importance of dark mode](tumour-snow-linkedin.md) for how you can help_