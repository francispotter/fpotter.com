# Miscellanea

- [Software side projects](https://steamwiz.io) - The "Steampunk Wizard project", a collection of command-line personal productivity tools that help life go a little more smoothly
- [Busy](https://busy.steamwiz.io) - "Personal time management for techies" - a CLI todo-list tool I developed during downtime and use every day
- [WizLib](https://wizlib.steamwiz.io/) - Dramework for command-line devops and personal productivity tools in Python
- [Video portfolio](https://www.youtube.com/playlist?list=PLsxxpZZ4jDu7NE9azj-yNwGT23zzb0Utu) - A variety of conference talks and explainer videos on technical topics, mostly about GitLab and DevOps, with some AI and ML
- [LinkedIn profile](https://www.linkedin.com/in/francispotter/) - Decades of diverse experience in the universe of computing technology
- [ProCICD guide](https://procicd.gitlab.io/) - A notebook of CI/CD best practives developed to meet a series of personal and professional requirements
- [ProCICD base library](https://gitlab.com/procicd/lib) - Patterns and conventions for organization-specific CI/CD libraries
- [My GitLab profile](https://gitlab.com/francispotter) - Reflects personal projects and contributions
- [Paul Strasburg and the history of IDEX](https://idex.fpotter.com/) - content developed for a 1996 fundraising event for a favourite social justice organization
- [Sally](https://www.lulu.com/shop/sally-dvorak/sally/paperback/product-eyvzgp.html) - a book I published
