# Writings

### [Surviving a brain tumour](tumour-snow-email.md)

In the early evening of November 10, 2022, in a small room in the emergency area of North Island Hospoital Comox Valley, a doctor entered and showed me a picture of the inside of my brain. My world changed in an instant...

### [A vision of static](tumour-snow-vsi.md)

Colours appeared less vibrant, as I had noticed in the store. But I also saw static — like an old-fashioned television with a broken antenna — across my entire visual field, distorting everything...

###  [The importance of dark mode](tumour-snow-linkedin.md) 

Consider using a dark background for presentation slides and other applications that support it. You might appreciate giving your eyes a break, even for everyday work, and it would certainly help when sharing with me...

### [The Transformation Architect](transformation-architect.md)

Rooms buzzed with creative energy and enthusiasm as individuals broke from their daily grind to brainstorm and collaborate in a deep, powerful way. Frank unleashed the genius within every individual in the otherwise boring corporate environment, as ideas flowed...

### ["Selling is so easy!"](engineers-become-salespeople.md)

Evan turned, looked me in the eyes, grinned from ear to ear, and shouted — SCREAMED at me — “I am by far the BEST EVER!!”...

### [My professional ethics](professional-ethics.md)

While attending Carnegie Mellon University in the late 1980s, I attended two on-campus presentations by author Kurt Vonnegut. At the time, the university received large research grants each year from the US military...
