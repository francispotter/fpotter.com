# Wrangle shell environments using subshells

Summary:

- I love working in the terminal
- Commands and scripts can't set environment variables in the shell itself
- Frameworks and tools (such as `venv` in ) tend to recommend adding 