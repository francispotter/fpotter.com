# Distributed dev teams that actually work

_Originally published on Medium in Jul 2016_

_For a more contemporary perspective on distributed teams, see [GitLab's Guide to All-Remote](https://handbook.gitlab.com/handbook/company/culture/all-remote/guide/)_

---

We know that the world is full of brilliant developers who can elevate our products and enhance our companies. But how do you find a talented coder in Siberia, hire someone in Pakistan and manage a team in Cambodia from your office in the States?

Distributed development teams are as commonplace as they are complicated. Companies have seized on this model as a way to save on salary, recruit globally and enjoy 24/7 productivity. What they quickly realize, however, is that recruiting and managing developers across the globe takes work.

The battlefield of business is littered with failure. After several decades in this business, we’ve distilled the key to success into three essential secrets:

## Create a Force Multiplier

You are smart. Your stateside team is smart. Your distributed development team should amplify all that brainpower and act as a force multiplier, with foreign developers collaborating with local engineers to strengthen the work. So before you assemble a team, you’ll want to assess the skills you have locally and the project at hand.

For instance, my business is too robust for me to do all the coding myself, but I need to be able to scale project by project. This means I can’t employ full-time software engineers. I have to outsource.

Working with overseas developers means my company literally works around the clock for our clients. I’ll cue something up for before I leave the office in the evening, and I’ll wake up in the morning to find it done.

There are several platforms to assist companies in identifying and screening developers, tracking their work and handling international payment. Upwork, Rent A Coder and other companies allow U.S. managers to vet reliable, consistent freelancers who understand English. They offer tools to allow you to track freelancers’ progress and handle international payment.

## Test for Comprehension

Once you’ve identified the need and secured a platform for hiring, the real work begins. Start by screening rigorously.

While freelance-hiring platforms screen for technical skills (they provide tests, ratings and information about how much work a developer has done), there are equally important qualities to look for that you will have to assess yourself.

Language is the most important one. Distributed development teams require a level of over-communication, so English language comprehension is essential. It is far more important that a freelancer be able to understand English than speak it, so I always do a verbal interview on Skype to gauge comprehension.

The next thing to look for in a developer is whether they think like a developer. For example, I expect developers to use Git through their shell or command prompt. If potential hires only know the buttons in their IDE, their understanding might be shallow, and that’s a red flag.

Once I’ve narrowed down my possible candidate pool using the platform’s screening tools and a Skype interview, I will pay a potential hire for a test task. It’s always something very small and very specific. The project itself is trivial, what I am looking for is how they set up the project. I’ll give them access to a Git repository with a project in it. They’ll need to take the project, set it up and then execute it. Once I see how they manage with this small task, I’ll know whether they’re ready to join our team.

## Embrace Micromanagement

The most effective distributed development teams are managed by U.S. developers. When a non-tech manager in the U.S. tries to work with developers in a foreign country too much can get lost in translation. However, even developers can fall into the trap of ambiguity.

When overseeing a team with members in several countries, broad assignments can be disastrous. Micromanagement, over-communication and diligent progress follow-up is essential to keep up the pace of your project.

Use a good ticketing system that designates exactly what each person on the team should be doing. The more detailed the requirements per ticket are, the less confusion there will be. Explain what order you need things to be in and exactly what is expected. When you’re sitting in the same room as someone, you can brainstorm and innovate together. But overseas, you cannot afford to be loose about the process.

While you have to be very specific about what you want, you’ll need to be a little relaxed about when you get it. Given the social and political realities of some of your freelancers’ home countries, it will be nearly impossible to expect something on a specific day, at a specific time. Instead, I have found it helpful to line up five or six tasks at a time and make commitments on a week-to-week basis.

That said, the ability to tap into a talented global community is invaluable. When people ask me how my business has been able to grow and succeed, I credit the gold mine of talent we’ve accessed across the planet.

I’d call it the secret to success, except the value of distributed development teams is no secret.