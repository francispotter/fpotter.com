# Summary

[Introduction](introduction.md)

- [Writings - index](writings.md)
- [Surviving a brain tumour](tumour-snow-email.md)
- [A vision of static](tumour-snow-vsi.md)
- [The importance of dark mode](tumour-snow-linkedin.md)
- [The Transformation Architect](transformation-architect.md)
- ["Selling is so easy!"](engineers-become-salespeople.md)
- [Tips for engineers entering sales](win-win-sales-for-engineers.md)
- [Professional Values](professional-values.md)
- [Miscellanea](miscellanea.md)