# "Selling is so easy!"

_Originally published on Medium in Sep 2016_

“The new sales guy is here,” said the CEO.

I’d been managing the engineering team at a San Francisco tech startup for a couple of years. We had customers, but sales were slow, and the CEO had decided it was time to up the game, so he hired Evan.

“You must be Evan,” I said politely when I first met him, putting on my best warm smile and extending my hand for a firm shake. “How are you?”

Like most engineers, I saw sales and marketing people as aliens whose presence in the technology world had to be tolerated. Sales, to me, was for people who weren’t smart enough to code, and who brought unreasonable demands and stupid questions to the engineers who did the “real work”. I had a pretty bad attitude.

Evan turned, looked me in the eyes, grinned from ear to ear, and shouted — SCREAMED at me — “I am by far the BEST EVER!!”

“All style and no substance”, I thought. And Evan’s style was outrageous. He shouted in almost every conversation. He ate only vegan food, attended board meetings barefoot, and cold-called CEOs of Fortune 500 companies for sport. He also told me that engineers could become incredibly successful salespeople, if we could just get over our attitude. I firmly disagreed.

I assumed Evan would fall short of expectations and disappear. But within a quarter, our company’s typical deal size had increased a hundredfold. So I decided to pay attention. Maybe I could learn something.

Evan had pitched our services to a midwestern manufacturing firm, and two representatives from their marketing department came to the Bay Area to check out our capabilities in person before sealing the deal. I offered to join the group taking the visitors out for dinner.

“Where are we going?” I asked Evan, expecting one of the nice seafood restaurants near Pier 39, or perhaps an Italian place in North Beach.

But Evan had a different idea. He insisted we take the midwesterners to Cafe Gratitude, the new age vegan restaurant, in the middle of what some considered a tough neighborhood, with rainbow murals on the walls, feel-good board games on the tables, and staffers standing on chairs reading a manifesto of peace.

Evan had a great time, but our guests hated the decidedly unbusinesslike atmosphere. “What are we doing here?” they asked nervously as casually dressed young people greeted us with hugs and servers brought plates of strange salads with stranger names.

As we left the restaurant, I pulled our valuable prospects aside and whispered quietly, “sorry about this… you can order a hamburger from room service when you get back to your hotel… it’s OK.”

We thought the deal was lost.

In fact, Evan closed the deal, and I was hooked. What did this guy know that I could learn? Was it true that I could learn to close big deals, while maintaining my engineering mindset and smarts?

When I relaunched The Hathersage Group last year, I decided to seek Evan out for advice on how to sell my services better.

I flew back up to San Francisco, rode BART out from downtown, and walked to the graffiti-covered warehouse where Evan runs a new venture-backed startup. I waited patiently in the lobby, which doubles as the web site tech team room. I brought an offering for the master — fresh vegan chocolate treats from Cafe Gratitude.

When finally ushered into Evan’s cluttered chamber, I sat down, took out a piece of paper, looked him in the eye, and said “I want to learn how to sell. Tell me what to do.”

“Oh Francis!” he said with a grin. “Selling is so easy!”

Evan gave me ideas to consider, books to read, and exercises to do. I wrote down everything he said, and followed his guidance to the letter.

So far, the results have been amazing. My company’s relaunch surpassed my wildest expectations. Business is booming, and the future looks bright.

Based on my experience, I believe that engineers can learn how to close deals — not as slimey “sales guys”, but as professionals with deep integrity. And we can have fun doing it too. It just takes a desire to learn new ideas and dedication to developing and practicing new skills.

And, of course, it takes a good attitude. Every day has to be the best day ever.

_See [Tips for engineers entering sales](win-win-sales-for-engineers.md)_