
# Introduction

<img src="francis-potter-head-shot-2023-320.png" width="220px"/>

As a Senior Professional Services Engineer with GitLab, I empower software organizations to achieve peak performance and cultivate generative cultures through AI-driven workflows, end-to-end automation, and robust application security

Previously I served as a Senior Solution Architect (presales) and Product Markering Manager (competitive intelligence). I first joined the GitLab team at the beginning of 2019.

Before GitLab, I ran a tiny independent professional services firm after serving for years as senior engineering manager and product manager for various tech startups. My career has touched the worlds of content delivery (CDN), marketing software, sports media, meteorology, translation, social media, print production, international social justice, and SETI.

Visit my LinkedIn profile (below) for a complete professional history.


<h3><a href="https://steamwiz.io" target="_blank" rel="noopener noreferrer" style="text-decoration: none;">
    Software
    <span style="margin-left: 3px;">↗</span>
</a></h3>

The "Steampunk Wizard" project, a collection of command-line personal productivity tools that help life go a little more smoothly (developed by me independently)

<h3><a href="https://www.youtube.com/playlist?list=PLsxxpZZ4jDu7NE9azj-yNwGT23zzb0Utu" target="_blank" rel="noopener noreferrer" style="text-decoration: none;">
    Video portfolio
    <span style="margin-left: 3px;">↗</span>
</a></h3>

A variety of conference talks and explainer videos on technical topics, mostly about GitLab and DevOps, with some AI and ML

<h3><a href="https://www.linkedin.com/in/francispotter/" target="_blank" rel="noopener noreferrer" style="text-decoration: none;">
    LinkedIn profile
    <span style="margin-left: 3px;">↗</span>
</a></h3>


Decades of diverse experience in the universe of computing technology

### Personal background

Brain tumour survivor, Pythonista, ally, aspie, Rotarian, musical theatre nut, MotoGP follower, husband, father, Canadian, and resident of the traditional territory of the K'ómoks people
